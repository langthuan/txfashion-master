package com.txfashion.btc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;

import com.google.gson.Gson;
import com.google.gson.JsonArray;


/**
 * Main
 * @author thuanlang
 *
 */
public class btc {
	public static  String url = "https://api-pub.bitfinex.com/v2/candles/trade:4h:tBTCUSD/hist?limit=10000";
	
	
	public static double openPrice 	= 17102.91;
	public static double closePrice 	= 16977.0;
	public static double hightPrice 	= 17324.0;
	public static double lowPrice 		= 16895;
	
	public static double differrence 	= 80.0;
	
	public static void main(String[] args) throws ClientProtocolException, IOException {
		//JSONObject data = getJson(new URL(url));
		
		String data = getH4Data(url);
		
		//String data = "[[1669968000000,16961,16951,16961,16946,2.2339707],[1669953600000,16454,16861,17109,16446,71.8877352],[1669939200000,16982,16906,17044,16865,116.15173813]]";
		
		
		
        // converting jsonStrig into object
		JsonArray convertedObject = new Gson().fromJson(data, JsonArray.class);
		//System.out.println(convertedObject);
		
		List<Map<Calendar, List<Double>>> dataList = new ArrayList<>();
		
		// convert from JSON to LIST
		List<Double> dataElement = null;
		Map<Calendar, List<Double>> priceDateMap =  null;
		for (int i = 0; i < convertedObject.size(); i ++) {
			dataElement = new ArrayList<>();
			priceDateMap = new HashMap<>();
			
			
			
			JsonArray element = (JsonArray) convertedObject.get(i);
			
			double openPrice = Double.parseDouble(String.valueOf(element.get(1)));
			double closePrice = Double.parseDouble(String.valueOf(element.get(2)));
			double hightPrice = Double.parseDouble(String.valueOf(element.get(3)));
			double lowPrice = Double.parseDouble(String.valueOf(element.get(4)));
			
			double candleBodyLength =  Math.abs(openPrice - closePrice);
			double candleLength 	= 	Math.abs(hightPrice - lowPrice);
			
			
		
			
			//Date date=new Date(String.valueOf(element.get(0)));
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(Long.valueOf(String.valueOf(element.get(0))));
			
			
			//dataElement.add(Double.parseDouble(String.valueOf(element.get(0)))); // date - time
			dataElement.add(openPrice);
			dataElement.add(closePrice);
			dataElement.add(hightPrice);
			dataElement.add(lowPrice);
			
			dataElement.add(candleBodyLength); // add candle body length to list
			dataElement.add(candleLength); // add candle length to list
			
			priceDateMap.put(calendar, dataElement);
			
			dataList.add(priceDateMap);
			
		}
		
		// find similar candle
		findSimilarCandle(openPrice, closePrice, hightPrice, lowPrice, dataList);
		
		
	}
	
	
	public static void findSimilarCandle(Double openPrice, Double closePrice, Double hightPrice, Double lowPrice, List<Map<Calendar, List<Double>>> dataList) {
		double candleBodyLength =  Math.abs(openPrice - closePrice);
		double candleLength 	= 	Math.abs(hightPrice - lowPrice);
		
		for (Map<Calendar, List<Double>> priceData : dataList) {
		
			
			// get 
			Object key = priceData.keySet().toArray()[0];
			List<Double> dataElement = priceData.get(key);
			
			Calendar date = (Calendar) key;
			
			if (dataElement.get(0) == openPrice) {
				System.out.println("TAO DAY: " + date.getTime());
				
			}
			
			//System.out.println(dataElement.get(4));
			if ( ((candleBodyLength < dataElement.get(4) + differrence) &&  (candleBodyLength > dataElement.get(4) - differrence)) && 
					((candleLength < dataElement.get(5) + differrence) &&  (candleLength > dataElement.get(5) - differrence))
					
					&& dataElement.get(0) > dataElement.get(1) // important
					
					) {
				
				
				System.out.println(date.getTime());
			}
		}
		
		
	}

	/**
	 * Get data from url
	 * 
	 * @param urlData
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public static String getH4Data(String urlData) throws ClientProtocolException, IOException {
		HttpGet httpGet = new HttpGet(urlData);
	    HttpClient client = HttpClients.createDefault();
	    HttpResponse httpResponse = client.execute(httpGet);
	    String content = IOUtils.toString(httpResponse.getEntity().getContent(), "UTF-8");
	    
	    return content;
		
	}
	
	// convert from milisecond time stam to data time.
	

}
