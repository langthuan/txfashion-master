package com.txfashion.order;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ExportOrder extends Constant {

	private static final String FILE_TEMPLATE = "order_template.xlsx";
	public static final String ORDER_FOLDER = "order_files";
	
	public static String ORDER_CODE = "475";
	
	public static void main(String[] args) throws IOException, EncryptedDocumentException, InvalidFormatException {
		String outputFile = "LT" + ORDER_CODE + ".xlsx";
		writeOrderFile(readOrderFolder(ORDER_CODE), outputFile);	
		System.out.println("GENERATE " + outputFile + " SUCCESSFULLY");
		// NORMAL PRODUCT
		System.out.println("HOODIE: " + hoodieCount  + "\n"
				+ "SWEATSHIRT: " + longSleeveCount + "\n"
				+ "T-SHIRT: " + tshirtCount + "\n"
				+ "ZIP HOODIE: " + zipHoodieCount + "\n"
				+ "DRESS: " + dressCount + "\n"
				+ "AUTO SUNSHADE: " + ssCount + "\n"
				+ "BOMBER JACKET: " + bomberJacketCount + "\n"
				+ "Tumbler: " + tumblerCount+ "\n"
				+ "Baseball Jersey: " + baseballJerseyCount + "\n"
				+ "NFL Jersey: " + nflJerseyCount + "\n"
				
				+ "JD13 SHOES: " + jd13SHOESCount + "\n"
				+ "SPRING SHOES: " +  springShoesCount + "\n"
				
				);
		
		System.out.println(
			"TOTAL MASK: "  + maskCount + "\n"
			+ "  NORMAL MASK ( "
			+ "1M3F: " + mask1M3FCount
			+ " ,3M9F: " + mask3M9FCount
			+ " ,5M15F: " + mask5M15FCount
			+ " ,10M30F: " + mask10M30FCount
			+ ")" + "\n"
			+ "  SPORT MASK ( "
			+ "1M2F: " + sportMask1M2F
			+ " ,3M6F: " + sportMask3M6F
			+ " ,5M10F: " + sportMask5M10F
			+ " ,10M20F: " + sportMask10M20F
			+ ")"
		);
		
		System.out.println(
			"RUG: " + rugCount  + " (" + "LAGRE: " + rugLagreCount + ", MEDIUM: " + rugMediumCount + ", SMALL: " + rugSmallCount + ")" + "\n"
			+ "QUILT: " + blanketCount  + " (" + "KING: " + blanketKingCount + ", QUEEN: " + blanketQueenCount + ", TWIN: " + blanketTwinCount + ")" + "\n"
			+ "BEDDING SET: " + beddingSetCount  + " (" + "KING: " + beddingSetKingCount + ", QUEEN: " + beddingSetQueenCount + ", FULL: " + beddingSetFullCount +", TWIN: " + beddingSetTwinCount + ")" + "\n"
		);
		
		
		System.out.println(
			"WOOL SWEATER: " + woolsweaterCount + "\n"
			+ "HOODIE MASK: " + hoodiemaskCount + "\n"
			+ "CAP: " + capCount + "\n"
			+ "KID HOODIE: " + kidhoodieCount + "\n"
			+ "POLO: " + poloCount + "\n"
			+ "SLEEVELESS HOODIE: " + sleevelessHoodieCount + "\n"
			+ "HAWAIIAN SHIRT: " + hawaiianShirtCount + "\n"
		);
		
		
		
		
		double price = 
				// NORMAL
				(hoodieCount * hoodiePrice) 
				+ (longSleeveCount * longSleevePrice)
				+ (tshirtCount * tshirtPrice)
				+ (zipHoodieCount * zipHoodiePrice) 
				+ (dressCount * dressPrice) 
				+ (ssCount * ssPrice)
				+ (bomberJacketCount * bomberJacketPrice)
				+ (tumblerCount * tumblerPrice) 
				+ (baseballJerseyCount * baseballJerseyPrice)
				+ (nflJerseyCount * nflJerseyPrice)
				
				// SHOES
				+ (jd13SHOESCount * jd13ShoesPrice) 
				+ (springShoesCount * springShoesPrice) 
				 
				// MASK
				+ (mask1M3FCount * mask1M3FPrice) // 1M3F
				+ (mask3M9FCount * mask3M9FPrice) // 3M9F
				+ (mask5M15FCount * mask5M15Price) // 5M15F
				+ (mask10M30FCount * mask10M30Price)  // 10M30F
				
				// SPORT MASK
				+ (sportMask1M2F * sportMask1M2FPrice)
				+ (sportMask3M6F * sportMask3M6FPrice)
				+ (sportMask5M10F * sportMask5M10FPrice)
				+ (sportMask10M20F * sportMask10M20FPrice)
				
				// RUG
				+ (rugSmallCount * rugSmallPrice)
				+ (rugMediumCount * rugMediumPrice)
				+ (rugLagreCount * rugLagrePrice)
				
				// BLANKET
				+ (blanketKingCount * blanketKingPrice)
				+ (blanketQueenCount * blanketQueenPrice)
				+ (blanketTwinCount * blanketTwinPrice)
				
				// Bedding
				+ (beddingSetTwinCount * beddingSetTwinPrice)
				+ (beddingSetFullCount * beddingSetFullPrice)
				+ (beddingSetQueenCount * beddingSetQueenPrice)
				+ (beddingSetKingCount * beddingSetKingPrice)
				
				// Others
				+ (woolsweaterCount * woolsweaterPrice) //d
				+ (hoodiemaskCount * hoodiemaskPrice) //d
				+ (capCount * capPrice)
				+ (kidhoodieCount * kidhoodiePrice)
				+	(poloCount*poloPrice)
				+	(sleevelessHoodieCount * sleevelessHoodiePrice)
				+	(hawaiianShirtCount * hawaiianShirtPrice);
				

		System.out.println("TOTAL ITEM: " + totalItems);
		System.out.println(" *** PRICE: " + price);
		
		System.out.println("DISTRICT SHIPPING: ");
		System.out.println(" DISTRICT 1: " + district1Count + " PRICE: " + (district1Count * DISTRICT_1_PRICE));
		System.out.println(" DISTRICT 2: " + district2Count + " PRICE: " + (district2Count * DISTRICT_2_PRICE));
		System.out.println(" DISTRICT 3: " + district3Count + " PRICE: " + (district3Count * DISTRICT_3_PRICE));
		
		double totalDistrictPrice = (district1Count * DISTRICT_1_PRICE) + (district2Count * DISTRICT_2_PRICE) + (district3Count * DISTRICT_3_PRICE);
		System.out.println(" *** TOTAL SHIPPING DISTRICT PRICE: " + totalDistrictPrice);
//		System.out.println("PRICE DETAILS: \n"
//				
//				// NORMAL
//				+ "-- HOODIE: " + (hoodieCount * hoodiePrice) +"\n"
//				+ "-- LONG SLEEVE: " + (longSleeveCount * longSleevePrice)  +"\n"
//				+ "-- T-SHIRT: " + (tshirtCount * tshirtPrice) +"\n"
//				+ "-- ZIP HOODIE: " + (zipHoodieCount * zipHoodiePrice)  +"\n"
//				+ "-- DRESS: " + (dressCount * dressPrice)  +"\n"
//				+ "-- AUTO SHUNSHADE: " + (ssCount * ssPrice) +"\n"
//				+ "-- BOMBER: " + (bomberJacketCount * bomberJacketPrice) +"\n"
//				+ "-- TUMBLE: " + (tumblerCount * tumblerPrice) +"\n"
//				+ "-- JERSEY: " + (baseballJerseyCount * baseballJerseyPrice) +"\n"
//				
//				// SHOES
//				+ "-- SPRING SHOES: " + (springShoesCount * springShoesPrice)  +"\n"
//				+ "-- JORDAN 13: " + (jd13SHOESCount * jd13ShoesPrice)  +"\n"
//				
//				 
//				// MASK
//				+ "-- NORMAL MASK 1M3F: " + (mask1M3FCount * mask1M3FPrice) +"\n" // 1M3F
//				+ "-- NORMAL MASK 3M9F: " + (mask3M9FCount * mask3M9FPrice) +"\n" // 3M9F
//				+ "-- NORMAL MASK 5M10F: " + (mask5M15FCount * mask5M15Price) +"\n" // 5M15F
//				+ "-- NORMAL MASK 10M30F: " + (mask10M30FCount * mask10M30Price) +"\n"  // 10M30F
//				
//				// SPORT MASK
//				+ "-- SPORT MASK 1M2F: " + (sportMask1M2F * sportMask1M2FPrice) +"\n"
//				+ "-- SPORT MASK 3M6F: " + (sportMask3M6F * sportMask3M6FPrice) +"\n"
//				+ "-- SPORT MASK 5M10F: " + (sportMask5M10F * sportMask5M10FPrice) +"\n"
//				+ "-- SPORT MASK 10M20F: " + (sportMask10M20F * sportMask10M20FPrice) +"\n"
//				
//				// RUG
//				+ "-- RUG SMALL: " + (rugSmallCount * rugSmallPrice) +"\n"
//				+ "-- RUG MEDIUM: " + (rugMediumCount * rugMediumPrice) +"\n"
//				+ "-- RUG LARGE: " + (rugLagreCount * rugLagrePrice) +"\n"
//				
//				// BLANKET
//				+ "-- BLANKET KING: " + (blanketKingCount * blanketKingPrice) +"\n"
//				+ "-- BLANKET QUEEN: " + (blanketQueenCount * blanketQueenPrice) +"\n"
//				+ "-- BLANKET TWIN: " + (blanketTwinCount * blanketTwinPrice) +"\n"
//				
//				// Bedding
//				+ "-- BEDDING TWIN: " + (beddingSetTwinCount * beddingSetTwinPrice) +"\n"
//				+ "-- BEDDING FULL: " + (beddingSetFullCount * beddingSetFullPrice) +"\n"
//				+ "-- BEDDING QUEEN: " + (beddingSetQueenCount * beddingSetQueenPrice) +"\n"
//				+ "-- BEDDING KING: " + (beddingSetKingCount * beddingSetKingPrice) +"\n"
//				
//				// Others
//				+ "-- WOOL SWEATER: " + (woolsweaterCount * woolsweaterPrice) +"\n" //d
//				+ "-- HOODIE MASK: " + (hoodiemaskCount * hoodiemaskPrice) +"\n" //d
//				+ "-- CAP: " + (capCount * capPrice) +"\n"
//				+ "-- KID HOODIE: " + (kidhoodieCount * kidhoodiePrice) +"\n"
//				+ "-- POLO: " + (poloCount*poloPrice)
//				
//				+ "");
		

	}

	public static void writeOrderFile(List<List<String>> contens, String outputFile)
			throws EncryptedDocumentException, InvalidFormatException, IOException {

		System.out.println(contens);
		// Obtain a workbook from the excel file
		FileInputStream fileInputStream = new FileInputStream(FILE_TEMPLATE);
		Workbook wb = new XSSFWorkbook(fileInputStream);

		// Get Sheet at index 0
		Sheet sheet = wb.getSheetAt(0);
		int rowCount = sheet.getPhysicalNumberOfRows();
		for (int i = 0; i < contens.size(); i++) {
			Row row = sheet.createRow(rowCount + 1);
			for (int j = 0; j < contens.get(i).size(); j++) {
				row.createCell(j).setCellValue(contens.get(i).get(j));
			}

			rowCount++;
		}

		FileOutputStream fileOut;
		try {
			fileOut = new FileOutputStream(outputFile, true);
			wb.write(fileOut);

			fileOut.close();
			wb.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static List<List<String>> readOrderFolder(String orderNo) throws IOException {
		File folder = new File(ORDER_FOLDER);
		File[] listOfFiles = folder.listFiles();

		List<List<String>> allContents = new ArrayList<List<String>>();
		for (File file : listOfFiles) {
			if (file.isFile()) {
				String fileName = ORDER_FOLDER + "/" + file.getName();
				allContents.addAll(readOrderFile(fileName, orderNo));
			}
		}

		return allContents;

	}

	/**
	 * Calculate Product type
	 * 
	 * @param variantTitle
	 * @param quality
	 */
	public static void calculateProductType(String variantTitle, int quantity) {
		
		if (variantTitle.contains(SLEEVELESS_HOODIE)) {
			// POLOCOUNT
			sleevelessHoodieCount = sleevelessHoodieCount +quantity;
			totalItems = totalItems + quantity;
		} else if (variantTitle.contains(HOODIE_MASK)) {
			// HOODIE MASK
			hoodiemaskCount = hoodiemaskCount + quantity; 
			totalItems = totalItems + quantity;
		}else if(variantTitle.contains(KID_HOODIE)) {
			// KIS HOODIE
			kidhoodieCount = kidhoodieCount + quantity;
			totalItems = totalItems + quantity;
		}
		else if (variantTitle.contains(ZIP_HOODIE)) {
			// ZIP HOODIE
			zipHoodieCount = zipHoodieCount + quantity;
			totalItems = totalItems + quantity;
		} else if (variantTitle.contains(LONG_SLEEVE)) {
			// LONG SLEVE
			longSleeveCount = longSleeveCount + quantity;
			totalItems = totalItems + quantity;
		} else if (variantTitle.contains(T_SHIRT)) {
			// TSHIRT
			tshirtCount = tshirtCount + quantity;
			totalItems = totalItems + quantity;
		} else if (variantTitle.contains(HOODIE)) {
			// HOODIE
			hoodieCount = hoodieCount + quantity;
			totalItems = totalItems + quantity;
		} else if (variantTitle.contains(JD13_SHOES)) {
			// JORDAN 13 SHOES
			jd13SHOESCount = jd13SHOESCount + quantity;
			totalItems = totalItems + quantity;
		}else if (variantTitle.contains(SPRING_SHOES)) {
			// SPRING SHOES
			springShoesCount = springShoesCount + quantity;
			totalItems = totalItems + quantity;
		}else if (variantTitle.contains(DRESS)) {
			// DRESS
			dressCount = dressCount + quantity;
			totalItems = totalItems + quantity;
		} else if (variantTitle.contains("Quilt")) {
			// QUILT
			blanketCount = blanketCount + quantity;
			totalItems = totalItems + quantity;
		} else if (variantTitle.contains(SUNSHADE) || variantTitle.contains(AUTOSHUNSHADE)) {
			// AUTO SUNSHADE
			ssCount = ssCount + quantity;
			totalItems = totalItems + quantity;
		}
		else if (variantTitle.contains(BOMBER_JACKET)) {
			// BOMBER
			bomberJacketCount = bomberJacketCount + quantity;
			totalItems = totalItems + quantity;
		} else if (variantTitle.contains(TUMBLER)) {
			// TUMBLER
			tumblerCount = tumblerCount + quantity;
			totalItems = totalItems + quantity;
		} else if (variantTitle.contains(BASEBALL_JERSEY)) {
			// BAEBALL JERSEY
			baseballJerseyCount = baseballJerseyCount + quantity;
			totalItems = totalItems + quantity;
		} else if (variantTitle.contains(MASK)) {
			// TOTAL MASK COUN
			maskCount = maskCount + quantity;
			totalItems = totalItems + quantity;
		} else if (variantTitle.contains(RUG)) {
			// RUG COUNT
			rugCount = rugCount + quantity;
			totalItems = totalItems + quantity;
		} else if (variantTitle.contains(BENDDING_SET)) {
			// BEDDING
			beddingSetCount = beddingSetCount + quantity;
			totalItems = totalItems + quantity;
		} else if (variantTitle.contains(WOOl_SWEATER)) {
			// WOLD SWEATER
			woolsweaterCount = woolsweaterCount +quantity;
			totalItems = totalItems + quantity;
		}else if(variantTitle.contains(Cap)){
			// CAP COUNT
			capCount = capCount +quantity;
			totalItems = totalItems + quantity;
		}//d
		else if (variantTitle.contains(POLO)) {
			// POLOCOUNT
			poloCount = poloCount +quantity;
			totalItems = totalItems + quantity;
		} else if (variantTitle.contains(NFL_JERSEY)) {
			// POLOCOUNT
			nflJerseyCount = nflJerseyCount +quantity;
			totalItems = totalItems + quantity;
		} else if (variantTitle.contains(HAWAIIAN_SHIRT)) {
			// POLOCOUNT
			hawaiianShirtCount = hawaiianShirtCount +quantity;
			totalItems = totalItems + quantity;
		} 
			
		
		
	}
	


	/**
	 * Read order file
	 * 
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	public static List<List<String>> readOrderFile(String fileName, String orderNo) throws IOException {
		Charset charset = Charset.forName("UTF-8");
		Reader reader = Files.newBufferedReader(Paths.get(fileName), charset);
		CSVParser csvParser = new CSVParser(reader,
				CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());

		List<List<String>> contents = new ArrayList<List<String>>();
		for (CSVRecord csvRecord : csvParser) {
			if ("Processing".equalsIgnoreCase(csvRecord.get("Order Status")) || "Completed".equalsIgnoreCase(csvRecord.get("Order Status")) || "On Hold".equalsIgnoreCase(csvRecord.get("Order Status")) ) {
				
		
				List<String> rowContent = new ArrayList<String>();
				// Accessing values by Header names
				rowContent.add(orderNo + "LT" + csvRecord.get("Order Number"));
				String sku 			= csvRecord.get("SKU");
				String temp[] 		= sku.split("-");
				String designCode 	= temp[0];
				String prodyctType 	= getProductType(temp[1]);
				String productSize 	= "";
				// get size for SHOES
				if (MASK.equals(prodyctType)) {
					// get size
					String productSizeCode = temp[1].replace("MK", "");
					productSize = getMaskCombo(productSizeCode, Integer.parseInt(csvRecord.get("Quantity")));
					
				} else if (BENDDING_SET.equals(prodyctType)) {
					// get size
					productSize = getBeddingSetSize(temp[2], Integer.parseInt(csvRecord.get("Quantity")));
					
				} else if (RUG.equals(prodyctType)) {
					// get size
					productSize = getRugSize(temp[2], Integer.parseInt(csvRecord.get("Quantity")));
					
				} else if (SPORT_MASK.equals(prodyctType)) { // Sport MASK
					// get size
					String productSizeCode = temp[1].replace("AC", "");
					productSize = getSportMaskCombo(productSizeCode, Integer.parseInt(csvRecord.get("Quantity")));
					
				} else if (JD13_SHOES.equals(prodyctType) || DRESS.equals(prodyctType)) {
					// get size - Type: Shoes | Size: MEN US9-EU42 | _WCPA_order_meta_data:
					String productVariant = csvRecord.get("Product Variation");
					productVariant = productVariant.replace("|", "-");
					productSize = productVariant.split("- ")[1].replaceAll("Size:", "").trim().replaceAll(" US[0-9][0-9]-","/").replaceAll(" US[0-9]-","/").trim();
					
				}else if (SPRING_SHOES.equals(prodyctType) || DRESS.equals(prodyctType)) {
					// get size - Type: Shoes | Size: MEN US9-EU42 | _WCPA_order_meta_data:
					String productVariant = csvRecord.get("Product Variation");
					productVariant = productVariant.replace("|", "-");
					productSize = productVariant.split("- ")[1].replaceAll("Size:", "").trim().replaceAll(" US[0-9][0-9]-","/").replaceAll(" US[0-9]-","/").trim();
					
				}
				else if (WOOl_SWEATER.equals(prodyctType)) {
					String productVariant = csvRecord.get("Product Variation");
					productVariant = productVariant.replace("|", "-");
					productSize = productVariant.split("-")[1].replace("Size:", "").trim();
				}else if (HOODIE_MASK.equals(prodyctType)) {
					String productVariant = csvRecord.get("Product Variation");
					productVariant = productVariant.replace("|", "-");
					productSize = productVariant.split("-")[0].replace("US Size:", "").trim();
				}
				else if (Cap.equals(prodyctType)) {
					String productVariant = csvRecord.get("Product Variation");
					productVariant = productVariant.replace("|", "-");
					productSize = productVariant.split("-")[0].replace("size:", "").trim();
					}
				else if  (SUNSHADE.equals(prodyctType)) {
					// get size
					productSize = "UF";
					
				} else  if (BLANKET.equals(prodyctType)) {
					
					productSize = getBlanketSize(temp[2], Integer.parseInt(csvRecord.get("Quantity")));
					
					prodyctType = "Quilt";
				} else {
					productSize = temp[2];
				}
				
				
				String variantTitle = prodyctType + " / " + productSize;
				rowContent.add(designCode);
				rowContent.add(variantTitle); // attribue
				rowContent.add(csvRecord.get("Quantity"));
				rowContent.add("");
				rowContent.add(""); // empty data
				
				String address 	= csvRecord.get("Address 1&2 (Shipping)");
				String city 	= csvRecord.get("City (Shipping)");
				String stateCode 	= csvRecord.get("State Code (Shipping)");
				String zipCode 	= csvRecord.get("Postcode (Shipping)");
				String countryCode 	= csvRecord.get("Country Code (Shipping)");
				
				rowContent.add(csvRecord.get("First Name (Shipping)") + " " + csvRecord.get("Last Name (Shipping)")); // Name
				rowContent.add(address);
				rowContent.add(""); // Address 2
				rowContent.add(city);
	
				if (stateCode == null || stateCode.equals("")) {
					stateCode = countryCode;
				}
	
				rowContent.add(stateCode);
				rowContent.add(countryCode);
				rowContent.add(zipCode); // Zip code
				rowContent.add(""); // Post Image
				rowContent.add(""); // Something
				rowContent.add(csvRecord.get("Phone (Billing)"));
				rowContent.add(csvRecord.get("Email (Billing)"));
				rowContent.add(""); // Something
				rowContent.add(csvRecord.get("Image URL"));
				
				String productvari = csvRecord.get("Product Variation").replace("|", "//");
				if (productvari.contains("Your Name")) {
					String[] tp = productvari.split("//"); 
					rowContent.add(tp[tp.length - 2] + " | " + tp[tp.length - 1]);
				} 
				else if (productvari.contains("Any Name")) {
					String[] tp = productvari.split("//"); 
					rowContent.add(tp[tp.length - 2] + " | " + tp[tp.length - 1]);
				} 
				else if (productvari.contains("Uploaded Media"))  {
					String[] tp = productvari.split("//");
					rowContent.add("Uploaded Media : https://"+tp[tp.length - 2]);//d
				}else {
					rowContent.add("");
				}
				
	
				// Calculate product type
				calculateProductType(variantTitle, Integer.parseInt(csvRecord.get("Quantity")));
	
				// Calculate District Shipping Price
				calculateShippingDisctrictFee(countryCode);
				
				contents.add(rowContent);
			}
		}

		csvParser.close();

		return contents;

	}
	
	public static String getProductType(String productTypeCode) {
		if ("LMS".equals(productTypeCode)) {
			return HOODIE; 
		} else if ("ZIP".equals(productTypeCode)) {
			return ZIP_HOODIE;
		} else if ("WY".equals(productTypeCode)) {
			return LONG_SLEEVE;
		} else if ("TX".equals(productTypeCode)) {
			return T_SHIRT;
		} else if ("SH".equals(productTypeCode)) { // JORDAN 13 SHOES
			return JD13_SHOES;
		} else if ("SFCS".equals(productTypeCode)) { // SPRING SHOES
			return SPRING_SHOES;
		} else if ("DR".equals(productTypeCode)) {
			return DRESS;
		} else if ("BLANKET".equals(productTypeCode)) {
			return BLANKET;
		} else if ("SUNSHADE".equals(productTypeCode) || "AUTOSHUNSHADE".equals(productTypeCode)) {
			return SUNSHADE;
		} else if ("BJ".equals(productTypeCode)) {
			return BOMBER_JACKET;
		} else if ("TB".equals(productTypeCode)) {
			return TUMBLER;
		} else if ("JR".equals(productTypeCode)) {
			return BASEBALL_JERSEY;
		} else if (productTypeCode.contains("MK")) {
			return MASK;
		} else if (productTypeCode.contains("AC")) {
			return SPORT_MASK;
		} else if (productTypeCode.contains("RUG")) {
			return RUG;
		} else if ("BDS".equals(productTypeCode)) {
			return BENDDING_SET;
		} else if ("WS".equals(productTypeCode)) {
			return WOOl_SWEATER; //d
		}else if ("HOODIEMASK".equals(productTypeCode)) {
			return HOODIE_MASK; //d
		}else if("CAP".equals(productTypeCode)) {
			return Cap;
		}else if("KH".equals(productTypeCode)) {
			return KID_HOODIE;
		}else if ("POLO".equals(productTypeCode)) {
			return POLO;
		}else if ("NJR".equals(productTypeCode)) {
			return NFL_JERSEY;
		} else if ("TSLH".equals(productTypeCode)) {
			return SLEEVELESS_HOODIE;
		} else if ("HBS".equals(productTypeCode)) {
			return HAWAIIAN_SHIRT;
		}
		//d
		
		return "";
	}
	
	
	
	public static String getMaskCombo(String code, int quantity) {
		String maskCombo = "";
		if (code.contains("1M3F")) {
			maskCombo = "1 MASK & 3 FILTERS";
			mask1M3FCount = mask1M3FCount + quantity; 
		} else if (code.contains("3M9F")) {
			maskCombo = "3 MASK & 9 FILTERS";
			mask3M9FCount = mask3M9FCount + quantity; // 3M 9F
		} else if (code.contains("5M15F")) {
			maskCombo = "5 MASK & 15 FILTERS";
			mask5M15FCount = mask5M15FCount + quantity; // 5M 15 F
		} else  if (code.contains("10M30F")) {
			maskCombo = "10 MASK & 30 FILTERS";
			mask10M30FCount = mask10M30FCount + quantity; // 10M * 30F
		}
		
		return maskCombo;
	}
	
	
	public static String getRugSize(String code, int quantity) {
		String size = "";
		if (code.equalsIgnoreCase("SMALL")) {
			size = "150x90cm";
			rugSmallCount = rugSmallCount + quantity;
		} else if (code.equalsIgnoreCase("MEDIUM")) {
			size = "180x120cm";
			rugMediumCount = rugMediumCount + quantity;
		} else if (code.equalsIgnoreCase("LARGE") ||  code.equalsIgnoreCase("LAGRE")) {
			size = "240x150cm";
			rugLagreCount = rugLagreCount + quantity;
		}
		
		return size;
	}
	
	public static String getSportMaskCombo(String code, int quantity) {
		String maskCombo = "";
		if (code.contains("1M2F")) {
			maskCombo = "1 MASK & 2 FILTERS";
			sportMask1M2F = sportMask1M2F + quantity; 
		} else if (code.contains("3M6F")) {
			maskCombo = "3 MASK & 6 FILTERS";
			sportMask3M6F = sportMask3M6F + quantity; // 3M 9F
		} else if (code.contains("5M10F")) {
			maskCombo = "5 MASK & 10 FILTERS";
			sportMask5M10F = sportMask5M10F + quantity; // 5M 15 F
		} else  if (code.contains("10M20F")) {
			maskCombo = "10 MASK & 20 FILTERS";
			sportMask10M20F = sportMask10M20F + quantity; // 10M * 20F
		}
		
		return maskCombo;
	}
	
	
	public static String getBlanketSize(String type, int quantity) {
		String size = "";
		
		if ("TWIN".equalsIgnoreCase(type)) {
			size =  "150 x 180cm";
			blanketTwinCount = blanketTwinCount + quantity;
		} else if ("QUEEN".equalsIgnoreCase(type)) {
			size =  "178 x 203cm";
			blanketQueenCount = blanketQueenCount + quantity;
		} else if ("KING".equalsIgnoreCase(type)) {
			size =  "203 x 228cm";
			blanketKingCount = blanketKingCount + quantity;
		}
		return size;
	}
	
	public static String getBeddingSetSize(String type, int quantity) {
		if ("TWIN".equalsIgnoreCase(type)) {
			type ="173x218cm 3pcs";
			beddingSetTwinCount = beddingSetTwinCount + quantity;
		} else if ("QUEEN".equalsIgnoreCase(type)) {
			type = "228x228cm 3pcs";
			beddingSetQueenCount = beddingSetQueenCount + quantity;
		} else if ("KING".equalsIgnoreCase(type)) {
			type ="228x264cm 3pcs";
			beddingSetKingCount = beddingSetKingCount + quantity;
		} else if ("FULL".equalsIgnoreCase(type)) {
			type ="203x228cm 3pcs";
			beddingSetFullCount = beddingSetFullCount + quantity;
		}
		
		return type;
	}
	
	/**
	 * 
	 * @param content
	 * @return
	 */
	public static List<String> convertFromStringToLis(String content) {
		List<String> myList = new ArrayList<String>(Arrays.asList(content.split(",")));
		
		return myList;
	}
	
	/**
	 * Calculate Shipping District Fee
	 * 
	 * @param countryCode
	 */
	public static void calculateShippingDisctrictFee(String countryCode) {
		if (DISTRICT_1.contains(countryCode)) {
			district1Count ++;
		} else if (DISTRICT_2.contains(countryCode)) {
			district2Count ++;
		} else if (DISTRICT_3.contains(countryCode)) {
			district3Count ++;
		} else {
			district3Count ++;
		}
	}

}
