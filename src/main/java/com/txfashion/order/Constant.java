package com.txfashion.order;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Constant {
	
	public static int totalItems 			= 0;
	
	/**
	 * COUNT CONSTANT------------------------------------------------
	 */
	public static int hoodieCount 			= 0;
	public static int longSleeveCount 		= 0;
	public static int tshirtCount 			= 0;
	public static int zipHoodieCount 		= 0;
	public static int dressCount 			= 0;
	public static int ssCount 				= 0;
	public static int bomberJacketCount 	= 0;
	public static int tumblerCount 			= 0;
	public static int baseballJerseyCount 	= 0;
	public static int nflJerseyCount 	= 0;
	
	// SHOES
	public static int jd13SHOESCount 		= 0; // JORDAN 13 SHOES
	public static int springShoesCount 		= 0;
	
	// NORMAL MASK
	public static int maskCount 			= 0;
	public static int mask1M3FCount 		= 0;
	public static int mask3M9FCount 		= 0;
	public static int mask5M15FCount 		= 0;
	public static int mask10M30FCount 		= 0;
	
	// Sport MASK
	public static int sportMaskCount 		= 0;
	public static int sportMask1M2F 		= 0;
	public static int sportMask3M6F 		= 0;
	public static int sportMask5M10F 		= 0;
	public static int sportMask10M20F 		= 0;
	
	//RUG
	public static int rugCount				= 0;
	public static int rugSmallCount			= 0;
	public static int rugMediumCount		= 0;
	public static int rugLagreCount			= 0;
	
	// BLANKET
	public static int blanketCount 			= 0;
	public static int blanketKingCount 		= 0;
	public static int blanketQueenCount 	= 0;
	public static int blanketTwinCount 		= 0;
	
	// BEDDING
	public static int beddingSetCount 		= 0;
	public static int beddingSetTwinCount	= 0;
	public static int beddingSetFullCount	= 0;
	public static int beddingSetQueenCount	= 0;
	public static int beddingSetKingCount	= 0;
	
	// OTHER
	public static int woolsweaterCount  	= 0;//d
	public static int hoodiemaskCount 		= 0;//d
	public static int capCount 				= 0; //d
	public static int kidhoodieCount 		= 0;
	public static int poloCount 			= 0;
	
	public static int sleevelessHoodieCount	= 0;
	public static int hawaiianShirtCount	= 0;
	
	
	/**
	 * Price Constant-------------------------------------------------------
	 */
	// BASIC PRODUCT
	public static double hoodiePrice 			= 19;
	public static double longSleevePrice 		= 16.5;
	public static double tshirtPrice 			= 13;
	public static double zipHoodiePrice 		= 21;
	public static double dressPrice 			= 15;
	public static double ssPrice				= 14.5;
	public static double bomberJacketPrice 		= 22.5;
	public static double tumblerPrice 			= 17.5;
	public static double baseballJerseyPrice 	= 19;
	public static double nflJerseyPrice 		= 18;
	
	// SHOES
	public static double jd13ShoesPrice 		= 43;
	public static double springShoesPrice 		= 30.5;
	
	// NORMAL MASK PRICE
	public static double mask1M3FPrice 			= 7;
	public static double mask3M9FPrice 			= 10;
	public static double mask5M15Price 			= 13;
	public static double mask10M30Price 		= 21;
	
	// SPORT MASK PRICE
	public static double sportMask1M2FPrice 		= 9;
	public static double sportMask3M6FPrice 		= 16;
	public static double sportMask5M10FPrice 		= 23;
	public static double sportMask10M20FPrice 		= 42.5;
	
	// RUG PRICE
	public static double rugSmallPrice				= 26.5;
	public static double rugMediumPrice				= 35.5;
	public static double rugLagrePrice				= 55;
	
	// BLANKET PRICE
	public static double blanketKingPrice 			= 32;
	public static double blanketQueenPrice 			= 27;
	public static double blanketTwinPrice 			= 23.5;
	
	// BEDDING SET PRICE
	public static double beddingSetTwinPrice	= 0;
	public static double beddingSetFullPrice	= 0;
	public static double beddingSetQueenPrice	= 34.5;
	public static double beddingSetKingPrice	= 0;
	
	// OTHER
	public static double woolsweaterPrice  		= 23.5;//d
	public static double hoodiemaskPrice 		= 22.5;//d
	public static double capPrice 				= 11;
	public static double kidhoodiePrice 		= 15.5;
	public static double poloPrice 				= 16.5;
	
	
	public static double sleevelessHoodiePrice = 17.5;
	public static double hawaiianShirtPrice 	= 11.5;
	/**
	 * NAME CONSTANT-----------------------------------------
	 */
	public static String HOODIE 				= "Hoodie";
	public static String LONG_SLEEVE 			= "Sweatshirt";
	public static String T_SHIRT 				= "T-shirt";
	public static String ZIP_HOODIE 			= "Zip Hoodie";
	public static String DRESS 					= "Dress";
	public static String BASEBALL_JERSEY 		= "Baseball Jersey";
	public static String NFL_JERSEY 			= "NFL Jersey";
	
	public static String JD13_SHOES 			= "AJ13W"; // JORDAN 13
	public static String SPRING_SHOES 			= "SFCS/W/Shoes";
	
	public static String BLANKET 				= "Blanket";
	public static String BOMBER_JACKET 			= "Bomber Jacket";
	public static String SUNSHADE 				= "AUTO SUNSHADE";
	public static String AUTOSHUNSHADE 			= "AUTO SUNSHARE"; // Ma cua HPM
	public static String TUMBLER 				= "Tumbler";
	
	public static String MASK 					= "Mask";
	public static String SPORT_MASK 			= "Sport Mask";
	
	public static String RUG					= "Rug";
	public static String BENDDING_SET			= "Bedding Set";
	public static String WOOl_SWEATER 			= "Wool Sweater"; //d
	public static String HOODIE_MASK 			= "Hoodie Mask"; //d
	public static String Cap 					= "HCC";
	
	public static String KID_HOODIE 			= "Kid Hoodie";
	public static String POLO 					= "Polo";
	public static String SLEEVELESS_HOODIE 		= "Sleeveless Hoodie";
	public static String HAWAIIAN_SHIRT 		= "Hawaiian Shirt";
	
	
	// DISTRICT PRICE
	public static String DISTRICT_1_STRING = "HK,MO,TW,US,UK,FR,AT,UA,VN,ES,IT,DE,HU,NZ,GR,PT,LU,IE,PL,BE,TR,SK,BG,KG,TM,TJ,NP,PH,AE,KH,SRB,OM,GE,KW,UZ,BD,MM,LA,BY,CZ,EE,LT,IN,HR,RO,LK,BN,KP,MV,QA,BH,VE,MT,SI,PK,LB,JO,BT,IR,PS,KZ,ID,JP,KR,MY,SG,TH";
	public static String DISTRICT_2_STRING = "AU,IL,NO,CH,SE,NL,DK,FI,RU,SA,MR,CL,LV,AZ,AM,IQ,AF,MD,CA";
	public static String DISTRICT_3_STRING = "BR,MX,FJ,AN,SR,PE,SB,CX,ZA,MK,RE,MH,PY,IS,AR,CY,CR,CO,MU,MA,AL,NG,UY,GH,BB,TZ,CV,SC,BW,GF,RW,BF,SZ,SN,BZ,TG,ET,MG,BJ,GD,GM,VU,GA,DJ,NR,AG,LS,NE,VA,TO,GQ,EH,TP,KM,PG,LC,SV,EC,NI,HN,JM,DZ,DO,EG,NA,PA,GY,MW,SM,SL,MP,BI,GN,TD,LY,HT,ML,ER,SO,TV,CF,SD,CU,LR,KI,BS,VI,PR,CI,AW,PM,KN,WS,CC,DM,GP,MQ,CG,SH,BM,BA,KY,CK,WF,PN,ASC,TC,VG,VC,FO,PF,GI,NC,NF,FK,GL,AI,ST";
	
	public static double DISTRICT_1_PRICE = 0;
	public static double DISTRICT_2_PRICE = 2;
	public static double DISTRICT_3_PRICE = 8;
	
	public static int district1Count = 0;
	public static int district2Count = 0;
	public static int district3Count = 0;
	
	public static List<String> DISTRICT_1 = new ArrayList<String>(Arrays.asList(DISTRICT_1_STRING.split(",")));
	public static List<String> DISTRICT_2 = new ArrayList<String>(Arrays.asList(DISTRICT_2_STRING.split(",")));
	public static List<String> DISTRICT_3 = new ArrayList<String>(Arrays.asList(DISTRICT_3_STRING.split(",")));
	
	public static double itemCost = 0;
	public static double shippingCost = 0;
	
	
	
}
