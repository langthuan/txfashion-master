package com.txfashion.fbads;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * 
 * @author langthuan
 *
 */
public class ExportData {

	public static final String FILE_PATH = "/Users/langthuan/Downloads/Received";

	public static void main(String[] args) throws IOException {

		// Copy all neccessary design code to folder.
		File folder = new File(FILE_PATH);
		File[] listOfFiles = folder.listFiles();

		List<List<String>> orderInfos = new ArrayList<List<String>>();
		for (File file : listOfFiles) {
			try {
				String fileName = file.getName();
				if (!fileName.equalsIgnoreCase(".DS_Store")) {
					orderInfos.addAll((readOrderFile(FILE_PATH + File.separator + file.getName())));
					
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		printData(orderInfos); // print data to CSV file
		System.out.println(orderInfos);
	}
	
	
	/**
	 * Print data to csv file
	 */
	public static void printData(List<List<String>> orderInfos ) throws IOException {
		BufferedWriter writer = Files.newBufferedWriter(Paths.get("fb-data-full.csv"));
		CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader("email","phone","image-link", "zip", "ct", "st","country"));
		
		System.out.println("TOTAL FILE: " + orderInfos.size());
		for (List<String> infos : orderInfos) {
			//System.out.println(infos);
			System.out.println(infos);
			csvPrinter.printRecord(infos.get(1), infos.get(0), infos.get(2), infos.get(6), infos.get(3), infos.get(4), infos.get(5));
			
		}
	}
	
	// generate csv file
	
	/**
	 * 
	 * Read files
	 * 
	 * @param fileName
	 * @param orderNo
	 * @return
	 * @throws IOException
	 */
	public static List<List<String>> readOrderFile(String fileName) throws IOException {
		fileName = fileName.replace("~$", "");
		System.out.println(fileName);
		FileInputStream file = new FileInputStream(new File(fileName));

		// Create Workbook instance holding reference to .xlsx file
		XSSFWorkbook workbook = new XSSFWorkbook(file);

		// Get first/desired sheet from the workbook
		XSSFSheet sheet = workbook.getSheetAt(0);

		Iterator<Row> rowIterator = sheet.iterator();
		List<List<String>> contents = new ArrayList<List<String>>();
		int index = 0;
		while (rowIterator.hasNext()) {
			// Get data
			Row row = rowIterator.next();
			
			if (index != 0) { // Ignore the fist row
				List<String> rowContent = new ArrayList<String>();
				
				// Exceoption
				Cell phoneCell = row.getCell(15); // get PHONE CELL
				if (phoneCell  != null); {
					try {
						rowContent.add(phoneCell.getStringCellValue());
					} catch (Exception e) {
						try {
							System.out.println("GET INT");
							rowContent.add(String.valueOf(phoneCell.getNumericCellValue()));
						} catch (Exception ex) {
							rowContent.add("");
						}
						
					}
				}
				
				// GET EMAIL
				Cell emailCell = row.getCell(16);
				if (emailCell != null) {
					rowContent.add(emailCell.getStringCellValue().trim()); // EMAIL
				} else {
					rowContent.add("");
				}
				
				// Get Image Link
				Cell imageLinkCell = row.getCell(18);
				if (imageLinkCell != null) {
					rowContent.add(imageLinkCell.getStringCellValue().trim());  // Image Link;
				} else {
					rowContent.add("");
				}
				
				// GET CITY
				Cell cityCell = row.getCell(9);
				if (imageLinkCell != null) {
					rowContent.add(cityCell.getStringCellValue().trim());  // GET CITY 3;
				} else {
					rowContent.add("");
				}
				
				// GET CITY CODE
				Cell cityCodeCell = row.getCell(10);
				if (imageLinkCell != null) {
					rowContent.add(cityCodeCell.getStringCellValue().trim());  // GET CITY CODE 4;
				} else {
					rowContent.add("");
				}
				
				// GET Country
				Cell countryCell = row.getCell(11);
				if (imageLinkCell != null) {
					rowContent.add(countryCell.getStringCellValue().trim());  // COUNTRY 5;
				} else {
					rowContent.add("");
				}
				
				// GET Country
				Cell zipcodeCell = row.getCell(12);
				if (imageLinkCell != null) {
					try {
						rowContent.add(zipcodeCell.getStringCellValue().trim());  // ZIP 6;
					} catch (Exception e) {
						rowContent.add(String.valueOf(zipcodeCell.getNumericCellValue()));  // ZIP 6;
					}
					
				} else {
					rowContent.add("");
				}
				
				if (!rowContent.isEmpty()) {
					if (rowContent.get(0).isEmpty() && rowContent.get(1).isEmpty() && rowContent.get(2).isEmpty()) {
						// do nothing
					} else {
						contents.add(rowContent);
					}
					
				} 
				
				
				
				
			}
			
			index ++;
			
			
			// For each row, iterate through all the columns
//			Iterator<Cell> cellIterator = row.cellIterator();
//			
//			while (cellIterator.hasNext()) {
//				
//				Cell cell = cellIterator.next();
//				rowContent.add(cell.getStringCellValue());
//				
//				contents.add(rowContent);
//			}
		}
		
		
		file.close();

		return contents;

	}

}
