package com.txfashion.crawdata;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class CrawData {

	public static String URL = "https://nflusa.com/product-tag/grinch-v2-nfl-full-team/page/5/";
	public static String LOCATION = "/Users/langthuan/Desktop/CRAWLER/HOODIE";
	public static int START_NUMBER = 245;

	public static void main(String[] args) throws IOException {
		Document document = Jsoup.connect(URL).get();

		// Get main Elements;

		Elements elms = document.getElementsByClass("shop-container");
		Element elm = elms.get(0);

		// get div image
		Elements divImages = elm.getElementsByClass("image-none");
		for (int i = 0; i < divImages.size(); i++) {
			Element aHref = divImages.get(i).getElementsByTag("a").first();
			Element image = aHref.getElementsByTag("img").first();

			String productName = aHref.attr("aria-label");
			String imageLink = image.attr("data-src").replace("-600x600", "");

			// Get team and product type.
			String[] temp = productName.split("–");
			
			int productNumber = i + START_NUMBER;
			String folderName = "DCC-GRILV" + productNumber + "-" + temp[2].replace("TEAM", "").replace("-", ",").trim();
			
			//System.out.println(productName);
			//System.out.println(folderName);
			//System.out.println(imageLink);
			
			String filePath = LOCATION +  "/" + folderName +  "/" + "hoodie_front.jpg";
		
			if (!folderName.contains("FLEECE")) {
				System.out.println(filePath);
				createFolderByName(folderName);
				downloadImage(imageLink, filePath);
			}
			
			

		}

		// System.out.println(divImages);
	}

	public static void createFolderByName(String name) throws IOException {
		String filePath = LOCATION + "/" + name;
		Path path = Paths.get(filePath);
		Files.createDirectory(path);
	}
	
	public static void downloadImage(String imageURL, String filePath) throws MalformedURLException, IOException {
		FileUtils.copyURLToFile(new java.net.URL(imageURL), new File(filePath));
	}

}
